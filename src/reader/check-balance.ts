import { Connection, LAMPORTS_PER_SOL, PublicKey } from "@solana/web3.js";

const suppliedPublicKey = process.argv[2];
if (!suppliedPublicKey) {
    throw new Error("It's mandatory to provide a wallet address");
}

let networktype = "devnet";
if (process.argv[3] === "mainnet") {
    networktype = "mainnet";
}

console.log(`Network selected -> ${networktype}`);

const networkEndpoint = networktype === "devnet" ? "https://api.devnet.solana.com" : "https://api.mainnet-beta.solana.com";

// self public key -> 7A85YhENcKynsJnd8tXdgu41hnMdirJzxCgrRtFaJU9i

let publicKey;

try {
    publicKey = new PublicKey(suppliedPublicKey);
    const connection = new Connection(networkEndpoint, "confirmed");
    const balanceInLamports = await connection.getBalance(publicKey);
    const balanceInSOL = balanceInLamports / LAMPORTS_PER_SOL;
    console.log(
        `💰 Finished! The balance for the wallet at address ${publicKey} is ${balanceInSOL}!`
        );
} catch {
    console.error("Invalid wallet address, please retry with a valid Solana address");
}

